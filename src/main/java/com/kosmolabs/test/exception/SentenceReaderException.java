package com.kosmolabs.test.exception;

public class SentenceReaderException extends RuntimeException {
    public SentenceReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
