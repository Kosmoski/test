package com.kosmolabs.test.model;

import lombok.Value;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Value
public class Sentence implements Serializable {
    private static final long serialVersionUID = 1L;

    int order;
    List<String> words;

    public Sentence(int order, List<String> words) {
        this.order = order;
        this.words = getSortedWords(words);
    }

    public int getWordsCount() {
        return words.size();
    }

    private List<String> getSortedWords(List<String> words) {
        List<String> sortedWords = new ArrayList<>(words);
        sortedWords.sort(String.CASE_INSENSITIVE_ORDER);

        return List.copyOf(sortedWords);
    }
}
