package com.kosmolabs.test.configuration;

import com.kosmolabs.test.service.SentencesOutputProcessorService;
import com.kosmolabs.test.service.output.SentencesOutputFormat;
import com.kosmolabs.test.service.parser.OpenNlpInputStreamSentenceParser;
import com.kosmolabs.test.service.parser.RegExInputStreamSentenceParser;
import com.kosmolabs.test.service.parser.WordTokenizer;
import com.kosmolabs.test.service.reader.FileCacheInputStreamSentenceReader;
import com.kosmolabs.test.service.reader.SentenceReader;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Configuration
public class SentenceOutputConfiguration {
    @Bean
    public SentenceDetectorME sentenceDetectorME(ResourceLoader resourceLoader) throws IOException {
        InputStream modelInputStream = resourceLoader.getResource("/models/en-sent.bin").getInputStream();
        SentenceModel model = new SentenceModel(modelInputStream);

        return new SentenceDetectorME(model);
    }

    @Bean
    public SentencesOutputProcessorService sentencesOutputProcessorService(SentencesOutputFormat sentencesOutputFormat,
                                                                           SentenceReader sentenceReader)  {

        return new SentencesOutputProcessorService(
                sentenceReader,
                sentencesOutputFormat,
                System.out);
    }

    @Primary
    @Bean
    public SentenceReader fileCacheInputStreamSentenceReaderUsingOpenNlpParser(SentenceDetectorME sentenceDetector,
                                                                               WordTokenizer wordTokenizer) throws IOException {

        File fileCache = File.createTempFile("sentence", ".tmp");
        fileCache.deleteOnExit();

        return new FileCacheInputStreamSentenceReader(
                System.in,
                new FileOutputStream(fileCache),
                new OpenNlpInputStreamSentenceParser(new FileInputStream(fileCache), sentenceDetector, wordTokenizer),
                new OpenNlpInputStreamSentenceParser(new FileInputStream(fileCache), sentenceDetector, wordTokenizer));
    }

    @Bean
    public SentenceReader fileCacheInputStreamSentenceReaderUsingRegExpParser(WordTokenizer wordTokenizer) throws IOException {

        File fileCache = File.createTempFile("sentence", ".tmp");
        fileCache.deleteOnExit();

        return new FileCacheInputStreamSentenceReader(
                System.in,
                new FileOutputStream(fileCache),
                new RegExInputStreamSentenceParser(new FileInputStream(fileCache), wordTokenizer),
                new RegExInputStreamSentenceParser(new FileInputStream(fileCache), wordTokenizer));
    }
}
