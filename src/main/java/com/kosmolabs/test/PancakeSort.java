package com.kosmolabs.test;

public class PancakeSort {

    /* Reverses arr[0..i] */
    static void flip(int arr[], int i)
    {
        for (int x = 0; x <= i / 2 ; ++x) {
            int temp = arr[x];
            arr[x] = arr[i - x ];
            arr[i - x] = temp;
        }
    }



    // The main function that
    // sorts given array using
    // flip operations
    static void pancakeSort(int arr[])
    {
        int size = arr.length - 1;

        while (size > 0) {
            int pos = findPositionOfBiggestElement(arr, size);

            flip(arr, pos);
            flip(arr, size);
            size--;
        }
    }

    static int findPositionOfBiggestElement(int arr[], int size) {
        int max = arr[0];
        int pos = 0;
        for (int i = 1; i <= size; ++i) {
            if (arr[i] > max) {
                max = arr[i];
                pos = i;
            }
        }

        return pos;
    }



    /* Utility function to print array arr[] */
    static void printArray(int arr[])
    {
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + " ");
        System.out.println("");
    }



    /* Driver function to check for above functions*/
    public static void main (String[] args)
    {
        int arr[] = {23, 10, 20, 11, 12, 6, 7};
        pancakeSort(arr);
        System.out.println("Sorted Array: ");
        printArray(arr);
    }
}