package com.kosmolabs.test.service.parser;

import com.kosmolabs.test.exception.SentenceReaderException;
import com.kosmolabs.test.model.Sentence;
import opennlp.tools.sentdetect.SentenceDetectorME;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Sentence parser implementation using Apache OpenNLP sentence detector
 */
public class OpenNlpInputStreamSentenceParser implements SentenceParser {
    private final InputStream inputStream;
    private final SentenceDetectorME sentenceDetector;
    private final WordTokenizer wordTokenizer;

    private final byte[] buffer;
    private final Deque<Sentence> bufferedSentences;

    private int sentenceCounter;
    private String lastNotCompletedSentence;
    private boolean isLastSentencedFlushed;

    public OpenNlpInputStreamSentenceParser(InputStream inputStream, SentenceDetectorME sentenceDetector, WordTokenizer wordTokenizer) {
        this.inputStream = inputStream;
        this.sentenceDetector = sentenceDetector;
        this.wordTokenizer = wordTokenizer;

        this.buffer = new byte[1024 * 1024];
        this.bufferedSentences = new LinkedList<>();
        this.lastNotCompletedSentence = "";
        this.isLastSentencedFlushed = false;
    }

    @Override
    public Sentence next() {
        return bufferedSentences.removeFirst();
    }

    @Override
    public boolean hasNext() {
        if (bufferedSentences.isEmpty()) {
            addNextSentencesToBuffer();
        }

        if (bufferedSentences.isEmpty()) {
            if (isLastSentencedFlushed) {
                return false;
            } else if ( !lastNotCompletedSentence.isBlank()) {
                bufferedSentences.add(createSentenceWithOrderFromString(++sentenceCounter, lastNotCompletedSentence));
                isLastSentencedFlushed = true;
            }
        }

        return !bufferedSentences.isEmpty();
    }

    private void addNextSentencesToBuffer() {
        for(;;) {
            Optional<String> nextStringFromInput = getNextStringFromInput();
            if (nextStringFromInput.isPresent()) {
                String buffer = lastNotCompletedSentence + nextStringFromInput.get();

                List<String> sentences = getSentencesFromBuffer(buffer);
                if (sentences.size() > 0) {
                    lastNotCompletedSentence = sentences.get(sentences.size() - 1);
                    for (int i = 0; i < sentences.size() - 1; ++i) {
                        bufferedSentences.add(createSentenceWithOrderFromString(++sentenceCounter, sentences.get(i)));
                    }
                }

                //At least 1 sentence was parsed and can be processed by reader
                if (bufferedSentences.size() > 0) {
                    break;
                }
            } else {
                //End of input stream
                break;
            }
        }
    }

    private Optional<String> getNextStringFromInput() {
        try {
            int numRead = inputStream.read(buffer);

            return numRead > 0 ? Optional.of(new String(buffer, 0, numRead)) : Optional.empty();
        } catch (IOException e) {
            throw new SentenceReaderException(e.getMessage(), e);
        }
    }

    private Sentence createSentenceWithOrderFromString(int order, String sentence) {
        return new Sentence(order, wordTokenizer.getWords(sentence));
    }

    private List<String> getSentencesFromBuffer(String buffer) {
        String[] detectedSentences = sentenceDetector.sentDetect(buffer);

        List<String> sentences = new LinkedList<>();
        for (String detectedSentence : detectedSentences) {
            //TODO It's probably only workaround and requires more investigation why OpenNLP doesn't treat "?" or "!" as sentence's end
            sentences.addAll(Arrays.stream(detectedSentence.split("[?!]+")).filter(s -> !s.isBlank()).collect(Collectors.toList()));
        }

        return sentences;
    }
}
