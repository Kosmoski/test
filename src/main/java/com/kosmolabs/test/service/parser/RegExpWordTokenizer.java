package com.kosmolabs.test.service.parser;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class RegExpWordTokenizer implements WordTokenizer {
    private static final String WORD_DELIMITER_PATTERN = "\\s+";
    private static final String REMOVE_CHARS_PATTERN = "([^\\p{IsAlphabetic}\\p{IsDigit}\\p{javaWhitespace}'’.]+)|(\\.{2,})";


    @Override
    public List<String> getWords(String text) {
        return List.of(text.replaceAll(REMOVE_CHARS_PATTERN, " ").trim().split(WORD_DELIMITER_PATTERN));
    }
}
