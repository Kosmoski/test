package com.kosmolabs.test.service.parser;

import java.util.List;

public interface WordTokenizer {
    List<String> getWords(String text);
}
