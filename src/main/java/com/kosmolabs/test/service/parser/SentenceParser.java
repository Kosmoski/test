package com.kosmolabs.test.service.parser;

import com.kosmolabs.test.model.Sentence;

public interface SentenceParser {
    Sentence next();
    boolean hasNext();
}
