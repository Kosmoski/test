package com.kosmolabs.test.service.parser;

import com.kosmolabs.test.model.Sentence;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Simple sentence parser implementation using scanner and regular expression
 */
public class RegExInputStreamSentenceParser implements SentenceParser {
    private static final String SENTENCE_DELIMITER_PATTERN = "[.!?]+(\\W*|\\n)?";

    private final Scanner scanner;
    private int sentenceCounter;
    private final WordTokenizer wordTokenizer;

    public RegExInputStreamSentenceParser(InputStream inputStream, WordTokenizer wordTokenizer) {
        scanner = new Scanner(inputStream).useDelimiter(SENTENCE_DELIMITER_PATTERN);
        this.wordTokenizer = wordTokenizer;
    }

    @Override
    public Sentence next() {
        return new Sentence(++sentenceCounter, wordTokenizer.getWords(scanner.next()));
    }

    @Override
    public boolean hasNext() {
        return scanner.hasNext();
    }
}
