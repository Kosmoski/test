package com.kosmolabs.test.service.output;

import com.kosmolabs.test.model.Sentence;
import org.springframework.stereotype.Component;


@ConditionalOnXmlSentenceOutputFormatEnabled
@Component
public class XmlSentenceOutputFormat implements SentencesOutputFormat {
    @Override
    public String getHeader(int maxWordsCountInSentence) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<text>\n";
    }

    @Override
    public String getSentence(Sentence sentence) {
        StringBuilder sentenceStringBuilder = new StringBuilder("<sentence>\n");
        for (String word : sentence.getWords()) {
            sentenceStringBuilder.append(String.format("<word>%s</word>\n", word));
        }

        return sentenceStringBuilder.append("</sentence>\n").toString();
    }

    @Override
    public String getFooter() {
        return "</text>";
    }
}
