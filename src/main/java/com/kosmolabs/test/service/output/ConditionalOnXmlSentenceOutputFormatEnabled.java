package com.kosmolabs.test.service.output;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@ConditionalOnProperty(value = "sentence.output.format", havingValue = "xml", matchIfMissing = true)
@Retention(RetentionPolicy.RUNTIME)
@interface ConditionalOnXmlSentenceOutputFormatEnabled {
}
