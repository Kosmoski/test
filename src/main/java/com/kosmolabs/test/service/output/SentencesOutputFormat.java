package com.kosmolabs.test.service.output;

import com.kosmolabs.test.model.Sentence;

public interface SentencesOutputFormat {
    String getHeader(int maxWordsCountInSentence);
    String getSentence(Sentence sentence);
    String getFooter();
}
