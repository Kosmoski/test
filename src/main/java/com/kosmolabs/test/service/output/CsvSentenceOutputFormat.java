package com.kosmolabs.test.service.output;

import com.kosmolabs.test.model.Sentence;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

@ConditionalOnCsvSentenceOutputFormatEnabled
@Component
public class CsvSentenceOutputFormat implements SentencesOutputFormat {
    @Override
    public String getHeader(int maxWordsCountInSentence) {
        return String.format(", %s\n", prepareColumnsWithWords(maxWordsCountInSentence));
    }

    @Override
    public String getSentence(Sentence sentence) {
        return String.format("Sentence %d, %s\n", sentence.getOrder(), String.join(", ", sentence.getWords()));
    }

    @Override
    public String getFooter() {
        return "";
    }

    private String prepareColumnsWithWords(int maxWordsCountInSentence) {
        return IntStream.range(0, maxWordsCountInSentence)
                        .boxed()
                        .map(i -> String.format("Word %d", i + 1))
                        .collect(Collectors.joining(", "));
    }
}
