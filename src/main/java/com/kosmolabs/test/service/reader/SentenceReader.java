package com.kosmolabs.test.service.reader;

import com.kosmolabs.test.model.Sentence;

public interface SentenceReader {
    Sentence next();
    boolean hasNext();
    int getMaxWordsCountInSentence();
}
