package com.kosmolabs.test.service.reader;

import com.kosmolabs.test.exception.SentenceReaderException;
import com.kosmolabs.test.model.Sentence;
import com.kosmolabs.test.service.parser.SentenceParser;
import lombok.RequiredArgsConstructor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@RequiredArgsConstructor
public class FileCacheInputStreamSentenceReader implements SentenceReader {

    private final InputStream sentenceInputStream;
    private final FileOutputStream fileCacheOutputStream;
    //Sentence parser used to get max words count from all sentences;
    private final SentenceParser fileCacheMaxWordsCountInSentenceParser;
    //Sentence parser used to output all sentences to output
    private final SentenceParser fileCacheOutputFormatSentenceParser;

    private boolean isFileCacheInitialized;
    private Integer maxWordsCountInSentence;

    @Override
    public int getMaxWordsCountInSentence() {
        initializeFileCache();

        if (maxWordsCountInSentence == null) {
            maxWordsCountInSentence = 0;

            while (fileCacheMaxWordsCountInSentenceParser.hasNext()) {
                Sentence sentence = fileCacheMaxWordsCountInSentenceParser.next();

                maxWordsCountInSentence = sentence.getWordsCount() > maxWordsCountInSentence
                        ? sentence.getWordsCount() :
                        maxWordsCountInSentence;
            }
        }

        return maxWordsCountInSentence;
    }

    @Override
    public Sentence next() {
        initializeFileCache();

        return fileCacheOutputFormatSentenceParser.next();
    }

    @Override
    public boolean hasNext() {
        initializeFileCache();

        return fileCacheOutputFormatSentenceParser.hasNext();
    }

    private void initializeFileCache() {
        if (!isFileCacheInitialized) {
            try {
                sentenceInputStream.transferTo(fileCacheOutputStream);
                isFileCacheInitialized = true;
            } catch (IOException e){
                throw new SentenceReaderException(e.getMessage(), e);
            }
        }
    }
}
