package com.kosmolabs.test.service;

import com.kosmolabs.test.service.output.SentencesOutputFormat;
import com.kosmolabs.test.service.reader.SentenceReader;
import lombok.RequiredArgsConstructor;

import java.io.OutputStream;

@RequiredArgsConstructor
public class SentencesOutputProcessorService {
    private final SentenceReader sentenceReader;
    private final SentencesOutputFormat sentencesOutputFormat;
    private final OutputStream sentencesOutputStream;

    public void process() throws Exception {
        sentencesOutputStream.write(sentencesOutputFormat.getHeader(sentenceReader.getMaxWordsCountInSentence()).getBytes());

        while (sentenceReader.hasNext()) {
            sentencesOutputStream.write(sentencesOutputFormat.getSentence(sentenceReader.next()).getBytes());
        }

        sentencesOutputStream.write(sentencesOutputFormat.getFooter().getBytes());
    }
}
