package com.kosmolabs.test.service;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SentenceProcessorRunner implements CommandLineRunner {
    private final SentencesOutputProcessorService sentencesOutputProcessorService;

    @Override
    public void run(String... args) throws Exception {
        sentencesOutputProcessorService.process();
    }
}
