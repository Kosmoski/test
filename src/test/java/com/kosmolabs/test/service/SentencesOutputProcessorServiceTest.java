package com.kosmolabs.test.service;

import com.kosmolabs.test.model.Sentence;
import com.kosmolabs.test.service.output.SentencesOutputFormat;
import com.kosmolabs.test.service.reader.SentenceReader;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.OutputStream;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SentencesOutputProcessorServiceTest {

    @Mock
    private SentenceReader sentenceReader;

    @Mock
    private SentencesOutputFormat sentencesOutputFormat;

    @Mock
    private OutputStream sentencesOutputStream;

    @InjectMocks
    private SentencesOutputProcessorService sentencesOutputProcessorService;

    @Test
    void process() throws Exception {
        //given
        when(sentencesOutputFormat.getHeader(1)).thenReturn("Header");
        when(sentencesOutputFormat.getFooter()).thenReturn("Footer");
        when(sentencesOutputFormat.getSentence(any())).thenReturn("Sentence 1")
                                                      .thenReturn("Sentence 2");

        Sentence sentence1 = new Sentence(1, List.of("a", "b", "c"));
        Sentence sentence2 = new Sentence(2, List.of("d", "e", "f"));


        when(sentenceReader.getMaxWordsCountInSentence()).thenReturn(1);
        when(sentenceReader.hasNext()).thenReturn(true)
                                      .thenReturn(true)
                                      .thenReturn(false);
        when(sentenceReader.next()).thenReturn(sentence1)
                                   .thenReturn(sentence2);

        //when
        sentencesOutputProcessorService.process();

        //then
        verify(sentencesOutputFormat).getHeader(1);
        verify(sentencesOutputFormat).getSentence(sentence1);
        verify(sentencesOutputFormat).getSentence(sentence2);
        verify(sentencesOutputFormat).getFooter();

        verify(sentencesOutputStream).write("Header".getBytes());
        verify(sentencesOutputStream).write("Sentence 1".getBytes());
        verify(sentencesOutputStream).write("Sentence 2".getBytes());
        verify(sentencesOutputStream).write("Footer".getBytes());

        verify(sentenceReader, times(3)).hasNext();
        verify(sentenceReader, times(2)).next();
    }
}
