package com.kosmolabs.test.model;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SentenceTest {

    @Test
    void sentenceHasIdAndWordsInOrder() {
        //given
        List<String> wordsNotInOrder = List.of("x", "h", "b", "f", "a");

        //when
        Sentence sentence = new Sentence(1, wordsNotInOrder);

        //then
        assertThat(sentence.getOrder()).isEqualTo(1);
        assertThat(sentence.getWords()).containsExactly("a", "b", "f", "h", "x");
    }
}
