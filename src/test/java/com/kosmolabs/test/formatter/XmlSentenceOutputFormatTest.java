package com.kosmolabs.test.formatter;

import com.kosmolabs.test.model.Sentence;
import com.kosmolabs.test.service.output.XmlSentenceOutputFormat;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class XmlSentenceOutputFormatTest {
    private final XmlSentenceOutputFormat xmlSentenceOutputFormat = new XmlSentenceOutputFormat();

    @Test
    void getHeader() {
        assertThat(xmlSentenceOutputFormat.getHeader(1))
                .isEqualTo("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<text>\n");
    }

    @Test
    void getFooter() {
        assertThat(xmlSentenceOutputFormat.getFooter()).isEqualTo("</text>");
    }

    @Test
    void getSentence() {
        //given
        Sentence sentence = new Sentence(1, List.of("a", "b", "c"));

        //when
        String csvSentence = xmlSentenceOutputFormat.getSentence(sentence);

        //then
        assertThat(csvSentence).isEqualTo("<sentence>\n<word>a</word>\n<word>b</word>\n<word>c</word>\n</sentence>\n");
    }
}
