package com.kosmolabs.test.formatter;

import com.kosmolabs.test.model.Sentence;
import com.kosmolabs.test.service.output.CsvSentenceOutputFormat;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CsvSentenceOutputFormatTest {

    private final CsvSentenceOutputFormat csvSentenceOutputFormat = new CsvSentenceOutputFormat();

    @Test
    void getHeader() {
        assertThat(csvSentenceOutputFormat.getHeader(3)).isEqualTo(", Word 1, Word 2, Word 3\n");
    }

    @Test
    void getFooter() {
        assertThat(csvSentenceOutputFormat.getFooter()).isEmpty();
    }

    @Test
    void getSentence() {
        //given
        Sentence sentence = new Sentence(1, List.of("a", "b", "c"));

        //when
        String csvSentence = csvSentenceOutputFormat.getSentence(sentence);

        //then
        assertThat(csvSentence).isEqualTo("Sentence 1, a, b, c\n");
    }
}
