package com.kosmolabs.test.parser;

import com.kosmolabs.test.model.Sentence;
import com.kosmolabs.test.service.parser.RegExInputStreamSentenceParser;
import com.kosmolabs.test.service.parser.WordTokenizer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RegExInputStreamSentenceParserTest {

    @Mock
    private WordTokenizer wordTokenizer;

    @Test
    void getNoneSentences() {
        //given
        String inputText = "";

        RegExInputStreamSentenceParser regExInputStreamSentenceParser =
                new RegExInputStreamSentenceParser(new ByteArrayInputStream(inputText.getBytes()), wordTokenizer);

        //when
        //then
        assertThat(regExInputStreamSentenceParser.hasNext()).isFalse();
    }

    @Test
    void getsAllSentences() {
        //given
        String inputText = "\"Mary had a little lamb. Peter called , and.\nCinderella likes shoes.\"";

        when(wordTokenizer.getWords(any())).thenReturn(List.of("a", "had", "lamb", "little", "Mary"))
                                           .thenReturn(List.of("and", "called", "Peter"))
                                           .thenReturn(List.of("Cinderella", "likes", "shoes"));

        RegExInputStreamSentenceParser regExInputStreamSentenceParser =
                new RegExInputStreamSentenceParser(new ByteArrayInputStream(inputText.getBytes()), wordTokenizer);

        //when
        //then
        assertThat(regExInputStreamSentenceParser.hasNext()).isTrue();
        Sentence sentence1 = regExInputStreamSentenceParser.next();
        assertThat(sentence1).isEqualTo(new Sentence(1, List.of("a", "had", "lamb", "little", "Mary")));

        assertThat(regExInputStreamSentenceParser.hasNext()).isTrue();
        Sentence sentence2 = regExInputStreamSentenceParser.next();
        assertThat(sentence2).isEqualTo(new Sentence(2, List.of("and", "called", "Peter")));

        assertThat(regExInputStreamSentenceParser.hasNext()).isTrue();
        Sentence sentence3 = regExInputStreamSentenceParser.next();
        assertThat(sentence3).isEqualTo(new Sentence(3, List.of("Cinderella", "likes", "shoes")));

        assertThat(regExInputStreamSentenceParser.hasNext()).isFalse();
    }
}
