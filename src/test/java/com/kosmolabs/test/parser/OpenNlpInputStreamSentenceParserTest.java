package com.kosmolabs.test.parser;

import com.kosmolabs.test.model.Sentence;
import com.kosmolabs.test.service.parser.OpenNlpInputStreamSentenceParser;
import com.kosmolabs.test.service.parser.WordTokenizer;
import opennlp.tools.sentdetect.SentenceDetectorME;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OpenNlpInputStreamSentenceParserTest {

    @Mock
    private SentenceDetectorME sentenceDetector;

    @Mock
    private WordTokenizer wordTokenizer;

    @Test
    void getNoneSentences() {
        //given
        String inputText = "";

       // when(sentenceDetector.sentDetect(any())).thenReturn(new String[] {});

        OpenNlpInputStreamSentenceParser openNlpInputStreamSentenceParser =
                new OpenNlpInputStreamSentenceParser(new ByteArrayInputStream(inputText.getBytes()),
                        sentenceDetector,
                        wordTokenizer);

        //when
        //then
        assertThat(openNlpInputStreamSentenceParser.hasNext()).isFalse();
    }

    @Test
    void getsAllSentences() {
        //given
        String inputText = "\"Mary had a little lamb. Peter called , and.\nCinderella likes shoes.\"";

        when(sentenceDetector.sentDetect(any())).thenReturn(new String[] {
                "Mary had a little lamb",
                "Peter called , and",
                "Cinderella likes shoes"});

        when(wordTokenizer.getWords(any())).thenReturn(List.of("a", "had", "lamb", "little", "Mary"))
                                           .thenReturn(List.of("and", "called", "Peter"))
                                           .thenReturn(List.of("Cinderella", "likes", "shoes"));

        OpenNlpInputStreamSentenceParser openNlpInputStreamSentenceParser =
                new OpenNlpInputStreamSentenceParser(
                        new ByteArrayInputStream(inputText.getBytes()),
                        sentenceDetector,
                        wordTokenizer);

        //when
        //then
        assertThat(openNlpInputStreamSentenceParser.hasNext()).isTrue();
        Sentence sentence1 = openNlpInputStreamSentenceParser.next();
        assertThat(sentence1).isEqualTo(new Sentence(1, List.of("a", "had", "lamb", "little", "Mary")));

        assertThat(openNlpInputStreamSentenceParser.hasNext()).isTrue();
        Sentence sentence2 = openNlpInputStreamSentenceParser.next();
        assertThat(sentence2).isEqualTo(new Sentence(2, List.of("and", "called", "Peter")));

        assertThat(openNlpInputStreamSentenceParser.hasNext()).isTrue();
        Sentence sentence3 = openNlpInputStreamSentenceParser.next();
        assertThat(sentence3).isEqualTo(new Sentence(3, List.of("Cinderella", "likes", "shoes")));

        assertThat(openNlpInputStreamSentenceParser.hasNext()).isFalse();
    }
}
