package com.kosmolabs.test.parser;

import com.kosmolabs.test.service.parser.RegExpWordTokenizer;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class RegExpWordTokenizerTest {
    private RegExpWordTokenizer regExpWordTokenizer = new RegExpWordTokenizer();

    @Test
    void getWords() {
        String text = "Mary had a little lamb. Peter called for\nthe wolf,and Aesop came.\n";

        List<String> words = regExpWordTokenizer.getWords(text);

        assertThat(words).containsExactlyInAnyOrder("Mary", "had", "little", "a", "lamb.", "Peter",
                                                    "called", "for", "the", "wolf", "and", "Aesop", "came.");
    }

}
