package com.kosmolabs.test.reader;

import com.kosmolabs.test.model.Sentence;
import com.kosmolabs.test.service.parser.SentenceParser;
import com.kosmolabs.test.service.reader.FileCacheInputStreamSentenceReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FileCacheInputStreamSentenceReaderTest {

    @Mock
    private InputStream sentenceInputStream;

    @Mock
    private FileOutputStream fileCacheOutputStream;

    @Mock
    private SentenceParser fileCacheMaxWordsCountInSentenceParser;

    @Mock
    private SentenceParser fileCacheOutputFormatSentenceParser;

    @InjectMocks
    private FileCacheInputStreamSentenceReader fileCacheInputStreamSentenceReader;

    @BeforeEach
    void setUp() {
        fileCacheInputStreamSentenceReader = new FileCacheInputStreamSentenceReader(
                sentenceInputStream,
                fileCacheOutputStream,
                fileCacheMaxWordsCountInSentenceParser,
                fileCacheOutputFormatSentenceParser);
    }

    @Test
    void getMaxWordsCountInSentence() throws IOException {
        //given
        when(fileCacheMaxWordsCountInSentenceParser.hasNext()).thenReturn(true)
                                                              .thenReturn(true)
                                                              .thenReturn(false);

        when(fileCacheMaxWordsCountInSentenceParser.next()).thenReturn(new Sentence(1, List.of("a", "b")))
                                                           .thenReturn(new Sentence(2, List.of("a", "b", "c")));

        //when
        int maxWordsCountInSentence = fileCacheInputStreamSentenceReader.getMaxWordsCountInSentence();

        //then
        assertThat(maxWordsCountInSentence).isEqualTo(3);

        verify(sentenceInputStream).transferTo(fileCacheOutputStream);

        verify(fileCacheMaxWordsCountInSentenceParser, times(3)).hasNext();
        verify(fileCacheMaxWordsCountInSentenceParser, times(2)).next();
    }

    @Test
    void getAllSentences() throws IOException {
        //given
        when(fileCacheOutputFormatSentenceParser.hasNext()).thenReturn(true)
                                                           .thenReturn(true)
                                                           .thenReturn(false);


        Sentence sentence1 = new Sentence(1, List.of("a", "b"));
        Sentence sentence2 = new Sentence(2, List.of("a", "b", "c"));

        when(fileCacheOutputFormatSentenceParser.next()).thenReturn(sentence1)
                                                        .thenReturn(sentence2);

        //when
        //then
        assertThat(fileCacheInputStreamSentenceReader.hasNext()).isTrue();
        assertThat(fileCacheInputStreamSentenceReader.next()).isEqualTo(sentence1);

        assertThat(fileCacheInputStreamSentenceReader.hasNext()).isTrue();
        assertThat(fileCacheInputStreamSentenceReader.next()).isEqualTo(sentence2);

        assertThat(fileCacheInputStreamSentenceReader.hasNext()).isFalse();

        verify(sentenceInputStream).transferTo(fileCacheOutputStream);
    }
}
