package com.kosmolabs.test;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PancakeSortTest {

    @Test
    void testIsFlipped() {
        int arr[] = {23, 10, 20, 11, 12, 6, 7};

        PancakeSort.flip(arr, 2);

        Assertions.assertThat(arr).containsExactly(20, 10, 23, 11, 12, 6, 7);
    }

    @Test
    void testSort() {
        int arr[] = {23, 10, 8, 20, 11, 12, 6, 7};

        PancakeSort.pancakeSort(arr);

        Assertions.assertThat(arr).containsExactly(6, 7, 8, 10, 11, 12, 20, 23);
    }

    @Test
    void testSortOnly1Element() {
        int arr[] = {23};

        PancakeSort.pancakeSort(arr);

        Assertions.assertThat(arr).containsExactly(23);
    }

    @Test
    void testSortEmptyArray() {
        int arr[] = {};

        PancakeSort.pancakeSort(arr);
    }

    @Test
    void  testFindMax() {
        int arr[] = {23, 10, 20, 11, 50, 12, 6, 7};

        Assertions.assertThat(PancakeSort.findPositionOfBiggestElement(arr, arr.length - 1)).isEqualTo(4);
    }
}
